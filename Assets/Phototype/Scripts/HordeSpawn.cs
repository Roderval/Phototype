﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HordeSpawn : MonoBehaviour {

	public GameObject enemy;
	public int numberOfEnemies = 10;
	public float minSpawnDelay = 1.5f;
	public float maxSpawnDelay = 2.5f;

	public bool blockAxisX = false;

	public void WakeUpHorde(){
		StartCoroutine (Spawner());
	}

	private IEnumerator Spawner(){
		for(int i = 0; i<numberOfEnemies; i++){

			Vector3 randPos;

			if (blockAxisX) {
				 randPos = gameObject.transform.position +
				                  new Vector3 (0, 0, Random.Range (-8, 8));
			} else {
				randPos = gameObject.transform.position +
					new Vector3 (Random.Range(-8,8), 0, Random.Range(-8,8));
			}
			
			Instantiate (enemy, randPos, new Quaternion(), gameObject.transform);
			yield return new WaitForSeconds (Random.Range(minSpawnDelay, maxSpawnDelay));
		}
	}

}
