﻿using UnityEngine;
using UnityEngine.UI;

public class Board : MonoBehaviour {

	public string stringName;
	public TextMesh text;

	void Awake(){
		text.text = LocalizationManager.instance.GetLocalizedValue(stringName);
	}

}