﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour {

	public GameSettings gameSettings;
	public static GameSettings gsObject = null;
	public bool renew = false;

	void Start(){

		Time.timeScale = 1f;

		if (!renew) {
			if (gsObject == null) {
				gsObject = Instantiate (gameSettings);
			}
		} else {
			Destroy (gsObject);
			gsObject = Instantiate (gameSettings);
		}
	}

}
