﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.Characters.FirstPerson;
using UnityEngine.SceneManagement;

public class StageFinalController : MonoBehaviour {

	public GameObject spawns;
	public MessagesController messages;
	public GameObject scenario;
	public GameObject finalSequenceUI;
	public AudioClip finalTrack;
	public Cannon cannon;
	public GameObject player;

	private bool isNewbie = true;
	private float startCurrent;

	void Start(){

		AudioManager.instance.PlayMusic ("music_boss");

		StartCoroutine (StartSequence());
		StartCoroutine (MiddleSequence());
		StartCoroutine (HelpSequence());
	}

	void FixedUpdate(){
		if(cannon.charge > 0){
			isNewbie = false;
		}
	}

	private IEnumerator StartSequence(){
		yield return new WaitForSeconds (3f);
		List<string> dialog = new List<string>();
        dialog.Add(LocalizationManager.instance.GetLocalizedValue("mission.finalStage.dialog01"));
        dialog.Add(LocalizationManager.instance.GetLocalizedValue("mission.finalStage.dialog02"));
        dialog.Add(LocalizationManager.instance.GetLocalizedValue("mission.finalStage.dialog03"));
        messages.SendDialog (dialog);
	}

	private IEnumerator MiddleSequence(){
		yield return new WaitForSeconds (25f);
		List<string> dialog = new List<string>();
        dialog.Add(LocalizationManager.instance.GetLocalizedValue("mission.finalStage.dialog04"));
        dialog.Add(LocalizationManager.instance.GetLocalizedValue("mission.finalStage.dialog05"));
        messages.SendDialog (dialog);
	}

	private IEnumerator HelpSequence(){
		yield return new WaitForSeconds (40f);
		if(isNewbie){
			List<string> dialog = new List<string>();
            dialog.Add(LocalizationManager.instance.GetLocalizedValue("mission.finalStage.dialog06"));
            dialog.Add(LocalizationManager.instance.GetLocalizedValue("mission.finalStage.dialog07"));
            messages.SendDialog (dialog);
		}
	}

	public void FinalSequence(){
		spawns.SetActive (false);
		StartCoroutine (StartFinalSequence());
	}

	public IEnumerator StartFinalSequence(){

		AudioManager.instance.LoopMusic ("music_titan", false);

		yield return new WaitForSeconds (4f);
		List<string> dialog = new List<string>();
        dialog.Add(LocalizationManager.instance.GetLocalizedValue("mission.finalStage.dialog08"));
        dialog.Add(LocalizationManager.instance.GetLocalizedValue("mission.finalStage.dialog09"));
        dialog.Add(LocalizationManager.instance.GetLocalizedValue("mission.finalStage.dialog10"));
        messages.SendDialog (dialog);
		yield return new WaitForSeconds (17f);
		scenario.SetActive (false);
		finalSequenceUI.SetActive (true);

		RigidbodyFirstPersonController controller = player.GetComponent<RigidbodyFirstPersonController> ();
		controller.mouseLook.lockCursor = false;
		Cursor.lockState = CursorLockMode.None;
		controller.enabled = false;

		yield return new WaitForSeconds (2f);
		//AudioManager.instance.PlayMusic ("music_end");
		SceneManager.LoadScene("FinalScene");
	}
}
