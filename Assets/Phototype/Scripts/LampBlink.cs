﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LampBlink : MonoBehaviour {

	public float minRange = 10;
	public float maxRange = 20;
	private Light lamp;
	private bool blink;

	// Use this for initialization
	void Start () {
		lamp = gameObject.GetComponent<Light> ();
		blink = true;
	}

	void FixedUpdate(){
		LampRange ();
	}

	private void LampRange(){
		if(blink){
			lamp.range = Mathf.Lerp (10f, 20f, Time.time / 10);
			if(lamp.range == maxRange){
				blink = false;
			}
		} else {
			lamp.range = Mathf.Lerp (20f, 10f, Time.time / 10);
			if(lamp.range == minRange){
				blink = true;
			}
		}
		Debug.Log (lamp.range);
	}
}
