﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimeSpawn : MonoBehaviour {

	public GameObject enemy;
	public GameObject spawns;
	public int numberOfEnemies = 20;
	public float minSpawnDelay = 10f;
	public float maxSpawnDelay = 20f;
	public float attackRange = 50f;

	public void Start(){
		StartCoroutine (Spawner());
	}

	private IEnumerator Spawner(){
		for(int i = 0; i<numberOfEnemies; i++){

			Transform spawnPoint = spawns.transform.GetChild (Random.Range (0, spawns.transform.childCount - 1));

			GameObject alien = Instantiate (enemy, spawnPoint.position, new Quaternion(), gameObject.transform);
			alien.GetComponent<EnemyController> ().attackRange = attackRange;
			Debug.Log ("Spawning enemy in " + spawnPoint.name + ": " + spawnPoint.position);
			yield return new WaitForSeconds (Random.Range(minSpawnDelay, maxSpawnDelay));
		}
	}
}
