﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EnemyController : Destroyable {

	public int maxHealth = 30;
	public int enemyDamage = 10;
	public float attackRange = 50f;
	public Material unspawnMaterial;
	public GameObject alien;
	public GameObject teleportSmoke;
	public AudioSource voice;
	public AudioClip deathSound;
	public int velocity;

	public GameObject dropItem;

	private float attackDelay = 1.0f;
	private float lastAttack = 0;
	private bool isAlive;
	private bool isMoving;
	private Renderer rend;
	private NavMeshAgent nav;
	private Animator anim;
	private AudioSource audioSource;
	private float lastMovingTime;
	private Collider alienCollider;

	private GameObject player;
	public int jumpPower = 50;

	private bool playerOnRange = false;
	private Coroutine attackInProgress;

	//DamageSound
	public AudioClip[] damageSounds;
	public AudioClip[] voiceSounds;

	//AttackSounds
	public AudioSource attackSound;

	void Start () {
		isAlive = true;
		lastMovingTime = Time.time;
		rend = alien.GetComponent<Renderer> ();
		nav = gameObject.GetComponent<NavMeshAgent>();
		anim = gameObject.GetComponent<Animator> ();
		alienCollider = gameObject.GetComponent<Collider> ();
		player = GameObject.FindGameObjectWithTag ("Player");
		audioSource = gameObject.GetComponent<AudioSource> ();

		AudioManager.instance.RegisterSpatialSource(audioSource);
		AudioManager.instance.RegisterSpatialSource(voice);
		AudioManager.instance.RegisterSpatialSource (attackSound);

		StartCoroutine ("Spawn");

		velocity = 4;
		nav.speed = velocity;
		anim.SetFloat ("walkSpeed", velocity);
	}

	void FixedUpdate(){

		if (isAlive) {
			if (Vector3.Distance (gameObject.transform.position, player.transform.position) < attackRange) {
				nav.SetDestination (player.transform.position);
			} else {
				RandomMovement();
			}
		} else {
			gameObject.transform.position += new Vector3 (0, 0.015f, 0);
			rend.material.SetColor("_TintColor", rend.material.GetColor("_TintColor") - new Color(0, 0, 0, 0.0035f));
		}

		if (nav.velocity.magnitude > 0) {
			anim.SetBool ("moving", true);
		} else {
			anim.SetBool ("moving", false);
		}
	}

	void OnTriggerStay(Collider other){
		if(other.CompareTag("Player") && isAlive){

			playerOnRange = true;

			if(Time.time - lastAttack > attackDelay){
				anim.SetBool ("onPlayerRange", true);
				attackSound.pitch = Random.Range (1f, 1.3f);
				attackSound.PlayDelayed(0.34f);
				lastAttack = Time.time;
				attackInProgress = StartCoroutine (InflictDamage());
			}
		}
	}

	void OnTriggerExit(Collider other){
		if(other.CompareTag("Player")){
			playerOnRange = false;
			anim.SetBool ("onPlayerRange", false);
		}
	}
	
	private IEnumerator InflictDamage(){
		//anim.SetBool ("onPlayerRange", true);
		yield return new WaitForSeconds(0.3f);
		if(playerOnRange){
			player.GetComponent<PlayerController> ().Damage (enemyDamage);
		}
	}

	public override void Damage(int damage){
		anim.SetTrigger ("hit");
		if (attackInProgress != null) {
			StopCoroutine (attackInProgress);
			attackSound.Stop ();
		}
		DamageSound ();
		maxHealth -= damage;

		if(maxHealth <= 0 && isAlive){
			rend.material.shader = Shader.Find("Particles/Additive");
			isAlive = false;
			StartCoroutine (Teleport());
		}
	}

	public IEnumerator Teleport(){

		GameSettings.instance.aliensScore++;

		anim.SetBool ("dead", true);
		nav.enabled = false;

		//Desativa colisor de damage
		gameObject.GetComponent<BoxCollider> ().enabled = false;

		alienCollider.enabled = false;

		playSound (deathSound);
		teleportSmoke.SetActive (true);
		teleportSmoke.GetComponent<ParticleSystem>().Play ();
		yield return new WaitForSeconds (3);
		gameObject.transform.localScale = new Vector3 (0, 0, 0);

		//DropItem
		//25% de chance de dropar a maleta medica
		if(Random.Range(0, 100) <= 25f){
			Instantiate(dropItem, gameObject.transform.position, new Quaternion(-90, 90, 0, 0));
		}

		yield return new WaitForSeconds (10);
		Destroy (gameObject);
	}

	private IEnumerator Spawn(){
		Shader originalShader = rend.material.shader;
		rend.material.shader = Shader.Find("Particles/Additive");
		yield return new WaitForSeconds (0.5f);
		rend.material.shader = originalShader;

		StartCoroutine(VoiceSound ());
	}

	private void playSound(AudioClip sound){
		audioSource.clip = sound;
		audioSource.Play();
	}

	private void RandomMovement(){
		if(Time.time - lastMovingTime > 5f){
			lastMovingTime = Time.time;
			Vector3 randomPosition = new Vector3 (Random.Range(-5f, 5f), 0, Random.Range(-5f, 5f));
			Debug.Log (randomPosition);
			nav.SetDestination (gameObject.transform.position + randomPosition);
		}
	}

	private void DamageSound(){
		voice.clip = damageSounds [Random.Range (0, damageSounds.Length)];
		voice.pitch = Random.Range (1.5f, 1.65f);
		voice.Play ();
	}

	private IEnumerator VoiceSound(){
		while(isAlive){
			if(attackInProgress == null){
				voice.clip = voiceSounds [Random.Range (0, voiceSounds.Length)];
				voice.pitch = Random.Range (1.0f, 1.1f);
				voice.Play ();
			}

			yield return new WaitForSeconds (Random.Range(3f, 5f));
		}
	}
}
