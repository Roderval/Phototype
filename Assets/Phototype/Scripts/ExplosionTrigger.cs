﻿using UnityEngine;

public class ExplosionTrigger : MonoBehaviour {

	public GameObject explosion;
	public PlumbingFix[] plumbing;
	private bool isActivated = false;

	void OnTriggerEnter(Collider other){
		if(other.CompareTag("Player")){
			if(!isActivated){
				explosion.SetActive (true);
				foreach(PlumbingFix p in plumbing){
					p.DestroyPlumbing ();
				}
			}
		}
	}

}
