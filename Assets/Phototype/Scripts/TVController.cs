﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TVController : MonoBehaviour {

	public GameObject plane;
	public int missionID;
	private GameSettings gameSettings;

	void Start () {

		GameObject obj = GameObject.FindGameObjectWithTag ("GameSettings");

		if(obj != null){
			gameSettings = obj.GetComponent<GameSettings> ();

			bool value = false;
			gameSettings.missionCompletedList.TryGetValue (missionID, out value);

			if(value){
				plane.SetActive(true);
			}	
		}
	}
}
