﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ClusterHUD : MonoBehaviour {

	public GridController cluster;

	public Text clusterName;
	public GameObject tensionBar;
	public GameObject currentBar;

	public Text tensionText;
	public Text currentText;

	private float maxTension = 380;
	private float maxCurrent = 90;


	void FixedUpdate(){

		clusterName.text = cluster.gridName;

		float tension = cluster.finalTension / maxTension;
		float current = cluster.finalCurrent / maxCurrent;

		tensionBar.transform.localScale = new Vector3 (tension, 1, 1);
		currentBar.transform.localScale = new Vector3 (current, 1, 1);

		tensionText.text = "TENSÃO: " + cluster.finalTension + "v/" + maxTension + "v";
		currentText.text = "CORRENTE: " + cluster.finalCurrent + "a/" + maxCurrent + "a";
	}

}
