﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SettingsDebugUI : MonoBehaviour {

	public Text debugText;

	GameSettings gameSettings;
	World world;

	void Start(){
		gameSettings = GameSettings.instance;
		world = World.instance;
	}
	
	void FixedUpdate () {
		debugText.text = "";

		debugText.text += "Stage " + world.stageID.ToString();
		debugText.text += "\nGame time: " + (Time.time - gameSettings.startTime).ToString("F2");
		debugText.text += "\nStage time: " + (Time.time - world.startTime).ToString("F2");
		debugText.text += "\nAliens: " + gameSettings.aliensScore.ToString();

		debugText.text += "\n= Finished =";
		foreach(KeyValuePair<int, bool> mc in gameSettings.missionCompletedList){
			debugText.text += mc.Key.ToString() + ": " + mc.Value.ToString() + "\n";  
		}
		
	}
}
