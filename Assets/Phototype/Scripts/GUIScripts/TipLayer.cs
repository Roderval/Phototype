﻿using UnityEngine;
using UnityEngine.UI;

public class TipLayer : MonoBehaviour {

	public Text tipText;

	public void SetText(string tip){
		tipText.text = tip;
	}
}
