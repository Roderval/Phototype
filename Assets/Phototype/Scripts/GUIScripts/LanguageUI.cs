﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LanguageUI : MonoBehaviour {

    public Dropdown languageDropdown;
    int index = 0;

    public void Start()
    {
        languageDropdown.onValueChanged.AddListener(delegate {
            index = languageDropdown.value;
            Debug.Log(languageDropdown.value);
        });
    }

    public void OnDisable()
    {
        switch (index)
        {
            case 0:
                {
                    PlayerPrefs.SetString("language", "default");
                    Debug.Log("saving default");
                    break;
                }
            case 1:
                {
                    PlayerPrefs.SetString("language", "english");
                    Debug.Log("saving english");
                    break;
                }
            default:
                {
                    break;
                }
        }
    }

}
