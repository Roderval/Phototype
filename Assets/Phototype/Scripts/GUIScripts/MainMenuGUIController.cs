﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenuGUIController : MonoBehaviour {

	public GameObject firstMenu;
    public GameObject languageScreen;
	public GameObject mainMenu;
	public GameObject startMenu;
	public GameObject creditsMenu;
	public GameObject optionsMenu;
	public GameObject loadingScreen;

	public GameObject textFirstMenu;

	private bool firstMenuActive = true;
	private Coroutine blinkCoroutine;
	private GameSettings gameSettings;

	void Start(){

        Time.timeScale = 1;

        blinkCoroutine = StartCoroutine (blinkFirstText());
		gameSettings = GameSettings.instance;
        ActiveMouse();

        //PlayerPrefs.DeleteAll();
    }

	void Update(){
		if (firstMenuActive) {
			if (Input.anyKey) {
				StopCoroutine (blinkCoroutine);
				firstMenuActive = false;
				firstMenu.SetActive (false);

                Debug.Log(PlayerPrefs.GetString("language", ""));
                //Testa se o user já selecionou uma lingua
                if (PlayerPrefs.GetString("language", "") == "")
                {
                    LanguageScreen();
                } else
                {
                    MainMenu();
                }
			}
		}
	}

	private IEnumerator blinkFirstText(){
		while (true) {
			textFirstMenu.SetActive (true);
			yield return new WaitForSeconds (1f);
			textFirstMenu.SetActive (false);
			yield return new WaitForSeconds (0.5f);
		}
	}

	public void MainMenu(){
        //PlayerPrefs.SetString("language", "pt_BR");

        startMenu.SetActive (false);
		creditsMenu.SetActive (false);
		optionsMenu.SetActive (false);
        languageScreen.SetActive(false);
		mainMenu.SetActive (true);
	}

    public void LanguageScreen()
    {
        startMenu.SetActive(false);
        creditsMenu.SetActive(false);
        optionsMenu.SetActive(false);
        languageScreen.SetActive(true);
        mainMenu.SetActive(false);
    }

	public void StartMenu(){
        if (SceneManager.sceneCountInBuildSettings > 2){
            creditsMenu.SetActive(false);
            optionsMenu.SetActive(false);
            mainMenu.SetActive(false);
            languageScreen.SetActive(false);
            startMenu.SetActive(true);
        } else{
            SceneManager.LoadScene(1);
        }
		
	}

	public void CreditsMenu(){
		startMenu.SetActive (false);
		optionsMenu.SetActive (false);
		mainMenu.SetActive (false);
        languageScreen.SetActive(false);
        creditsMenu.SetActive (true);
	}

	public void OptionsMenu(){
		startMenu.SetActive (false);
		creditsMenu.SetActive (false);
		mainMenu.SetActive (false);
        languageScreen.SetActive(false);
        optionsMenu.SetActive (true);
	}

	public void StartTutorial(){
		LoadingScreen ();
        SceneManager.LoadScene ("IntroScene");
    }

	public void StartExpertMode(){
		LoadingScreen ();
		gameSettings.missionCompletedList.Add (-1, true);
		SceneManager.LoadScene ("IntroScene");
	}

	public void LoadingScreen(){
		startMenu.SetActive (false);
		optionsMenu.SetActive (false);
		mainMenu.SetActive (false);
		creditsMenu.SetActive (false);
        languageScreen.SetActive(false);
        loadingScreen.SetActive (true);
	}

	public void ExitMenu(){
		Application.Quit ();
	}

    public void ActiveMouse()
    {
        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;
    }

}
