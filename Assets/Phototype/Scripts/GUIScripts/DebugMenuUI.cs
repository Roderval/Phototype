﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class DebugMenuUI : MonoBehaviour {

	public Text sceneText;

	public void LoadScene(){
		PauseMenu.instance.TransparentPause ();
		SceneManager.LoadScene (sceneText.text);
		gameObject.SetActive (false);
	}

	public void LoadScene(string name){
		PauseMenu.instance.TransparentPause ();
		SceneManager.LoadScene (name);
		gameObject.SetActive (false);
	}

	public void Close(){
		PauseMenu.instance.TransparentPause ();
		gameObject.SetActive (false);
	}

}