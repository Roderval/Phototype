﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.Characters.FirstPerson;
using UnityEngine.UI;

public class MouseOptions : MonoBehaviour {

	private RigidbodyFirstPersonController player;
	public Slider mouseSensitivity; 
	public Text msText;

	void Start(){

        if (GameObject.FindGameObjectWithTag("Player") != null)
        {
            player = GameObject.FindGameObjectWithTag("Player").GetComponent<RigidbodyFirstPersonController>();

            mouseSensitivity.value = GameSettings.instance.mouseSensivity;

            msText.text = mouseSensitivity.value.ToString("0.0");

            mouseSensitivity.onValueChanged.AddListener(
                delegate {
                    player.mouseLook.XSensitivity = mouseSensitivity.value;
                    player.mouseLook.YSensitivity = mouseSensitivity.value;
                    msText.text = mouseSensitivity.value.ToString("0.0");
                    GameSettings.instance.mouseSensivity = mouseSensitivity.value;
                }
            );
        } else
        {
            mouseSensitivity.value = GameSettings.instance.mouseSensivity;
            mouseSensitivity.onValueChanged.AddListener(
               delegate {
                   msText.text = mouseSensitivity.value.ToString("0.0");
                   GameSettings.instance.mouseSensivity = mouseSensitivity.value;
               }
           );
        }
	}



}
