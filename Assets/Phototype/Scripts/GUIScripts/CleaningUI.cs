﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CleaningUI : MonoBehaviour {

	public float cleaning;
	public Text text;
	public GameObject bar;
	public SolarPanel panel;

	public void SetPanel(SolarPanel panel){
		this.panel = panel;
	}

	void FixedUpdate(){
		text.text = panel.cleaning*100 + " / 100";
		bar.transform.localScale = new Vector3 (panel.cleaning, 1, 1);
	}

}
