﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class PauseMenuUI : MonoBehaviour {

	public GameObject confirmationMenu;
	public GameObject missionsMenu;
	public GameObject optionsMenu;
	public PauseMenu canvas;

	public void ConfirmationMenu(bool open){
		confirmationMenu.SetActive (open);
	}

	public void ExitGame(){
        GameSettings.instance.GameRestart();
	}

	public void ContinueGame(){
		canvas.PauseGame ();
	}

	public void MissionsMenu(bool open){
		missionsMenu.SetActive (open);
	}

	public void OptionsMenu(bool open){
		optionsMenu.SetActive (open);
	}

}
