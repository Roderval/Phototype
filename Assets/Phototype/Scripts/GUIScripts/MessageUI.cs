﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MessageUI : MonoBehaviour {

	public GameObject main;
	public GameObject speak;
	public Text mainMessage;
	public Text text;
	public List<string> textPool;

	private bool typing = false;

	public void StartText(){
		StartCoroutine (NextText());
	}

	public void ShowMessage(string message){
		mainMessage.text = message;
		main.SetActive (true);
		PauseMenu.instance.TransparentPause ();
	}

	public void CloseMainMessage(){
		mainMessage.text = "";
		main.SetActive(false);
		PauseMenu.instance.TransparentPause ();
	}

	private IEnumerator NextText(){

		speak.SetActive (true);

		while(textPool.Count > 0){
			//Debug.Log (textPool[0]);
			typing = true;
			StartCoroutine (TypeText(textPool [0]));

			while(typing){
				yield return new WaitForSeconds(1f);
			}

			yield return new WaitForSeconds(1.7f);

			textPool.RemoveAt (0);
		}

		Debug.Log ("Acabou!");
		speak.SetActive (false);
	}

	private IEnumerator TypeText(string dialog){
        AudioSource sound = AudioManager.instance.PlayEffect ("efx_typing");
		text.text = "";
		int i = 0;
		foreach(char c in dialog){
            text.text += c;
			i++;
			yield return new WaitForSeconds (0.05f);
		}
		sound.Stop ();
		typing = false;
	}

	public void setPool(List<string> textPool){
		StopAllCoroutines ();
		this.textPool = textPool;
        StartCoroutine (NextText());
	}
}
