﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraRotation : MonoBehaviour {

	public float cameraVelocity = 0.3f;
	
	// Update is called once per frame
	void Update () {
		Vector3 rot = new Vector3 (0, cameraVelocity, 0);
		gameObject.transform.Rotate(rot);
	}
}
