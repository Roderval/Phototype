﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class _deprecated_GridController : MonoBehaviour {

	private SolarPanelController[] photopanels;
	private LinkedList<CircuitConnection> connections;
	private LinkedList<GameObject> visualConnections;
	public GameObject GridLine;

	public float current = 0;
	public float tension = 0;

	void Start(){
		photopanels = gameObject.GetComponentsInChildren<SolarPanelController> ();
		connections = new LinkedList<CircuitConnection> ();
		visualConnections = new LinkedList<GameObject> ();

		float totalA = 0;
		float totalV = 0;

		SolarPanelController lastPanel = null;
		int i = 0;

		foreach (SolarPanelController panel in photopanels) {

			panel.idPanel = i + 1;

			totalA += panel.maxA;
			totalV += panel.maxV;

			if(i > 0){

				//visual
				GameObject grid = Instantiate (GridLine);
				visualConnections.AddFirst (grid);
				LineRenderer line = grid.GetComponent<LineRenderer> ();
				line.SetPosition (0, lastPanel.gameObject.transform.position);
				line.SetPosition (1, panel.gameObject.transform.position);

				//funcional
				connections.AddFirst (new CircuitConnection(lastPanel, panel, true));
				Debug.Log ("Criando conexão entre " + lastPanel.idPanel + " e " + panel.idPanel);
			}

			lastPanel = panel;
			i++;
		}

		connections.AddFirst (new CircuitConnection(lastPanel, null, true)); //last conn

		Debug.Log ("TotalA: " + totalA + " / TotalV: " + totalV);
	}

	void Update(){
		CalculatePower ();

		if(Input.GetKeyDown(KeyCode.I)){
			RemoveConnection (5);
		}
	}

	private void CalculatePower(){
		tension = 0;
		foreach(CircuitConnection c in connections){
			tension += c.c1.actualV;
		}
	}

	private void RemoveConnection(int id){

		CircuitConnection cRemove = null;
		GameObject vRemove = null;

		int i = 0;
		foreach(CircuitConnection c in connections){
			if(i == id){
				cRemove = c;
			}
			i++;
		}


		i = 0;
		foreach(GameObject v in visualConnections){
			if(i == id){
				vRemove = v;
			}
			i++;
		}

		//removing
		Debug.Log("Removing " + id);
		connections.Remove(cRemove);
		visualConnections.Remove (vRemove);

	}

}
