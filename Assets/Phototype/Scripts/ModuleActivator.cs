﻿using System.Collections;
using UnityEngine;

public class ModuleActivator : InteractableObject {

	public PlumbingFix[] plumbing;
	public Renderer button;
	public SolarGrid grid;

	public AudioSource audioSource;
	public AudioClip negateEffect;
	public AudioClip startEffect;

	public Animator anim;

    private string defaultTipText;
	private string tipText;

	void Start(){

        defaultTipText = LocalizationManager.instance.GetLocalizedValue("text.tip.moduleActivation");
        tipText = defaultTipText;

        AudioManager.instance.RegisterSpatialSource(audioSource);
	}

	public override string TipText ()
	{
		return tipText;
	}

	public override void triggerAction ()
	{

		bool isFixed = true;

		foreach(PlumbingFix p in plumbing){
			if (!p.isFixed) {
				isFixed = false;
			}
		}

		if (isFixed) {
			grid.isOn = true;
			button.material.color = Color.green;
			audioSource.clip = startEffect;
			anim.enabled = true;
			audioSource.Play ();

			isActivated = false;

		} else {
			audioSource.clip = negateEffect;
			audioSource.Play ();
			tipText = LocalizationManager.instance.GetLocalizedValue("text.tip.moduleActivationNegative");
            StopAllCoroutines ();
			StartCoroutine (RestartTip ());
		}
	}

	private IEnumerator RestartTip(){
		yield return new WaitForSeconds (3);
        tipText = defaultTipText;
    }

}
