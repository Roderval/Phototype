﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioRoomZone : MonoBehaviour {

	private AudioSource audioSrc;

	void Start(){
		audioSrc = gameObject.GetComponentInParent<AudioSource> ();
		AudioManager.instance.RegisterSpatialSource (audioSrc);
	}

	void OnTriggerEnter(){
		audioSrc.Play ();
	}

	void OnTriggerExit(){
		audioSrc.Stop ();
	}

}
