﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Stage02M1 : MonoBehaviour, IGoal {

	bool sucess = false;
	string goalName = "Ligando os pontos";
	string goalDesc = "Conecte as estações ao circuito em série";
	string goalLongDesc = "Utilize o botão próximo ao poste de energia para ativar a estação";

	public SolarGrid grid;

	void Awake(){
		string stationName = "X";

		goalName = "Ligando os pontos - " + stationName;
		goalDesc = "Conecte as estações ao circuito em série na estação " + stationName;
	}

	public IEnumerator StartGoal(System.Action<bool> done){
		while (true) {
			if (sucess) {
				done (true);
				break;
			}
			//Debug.Log ("Verificando " + sucess);
			yield return new WaitForSeconds (1);
		}
	}

	void FixedUpdate(){
		if(grid.tension > 380f){
			sucess = true;
		}
	}

	public void FinalizeGoal(){
		sucess = true;
	}

	public string GetName(){
		return goalName;
	}

	public string GetDesc(){
		return goalDesc;
	}

	public string GetLongDesc(){
		return goalLongDesc;
	}

	public bool GetCompleted(){
		return sucess;
	}
}
