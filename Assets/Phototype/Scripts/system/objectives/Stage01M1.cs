﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Stage01M1 : MonoBehaviour, IGoal {

	bool sucess = false;
	string goalName = "Limpeza";
	string goalDesc = "Limpe os painéis fotovoltaicos deste setor";
	string goalLongDesc = "Utilize a função limpeza da Multigun (botão esquerdo do mouse) para " +
		"limpar os painéis fotovoltaicos";

	public SolarGrid grid;

	void Start(){
		goalName = "Limpeza - " + grid.name;
	}

	public IEnumerator StartGoal(System.Action<bool> done){
		while (true) {
			if (sucess) {
				done (true);
				break;
			}
			//Debug.Log ("Verificando " + sucess);
			yield return new WaitForSeconds (1);
		}
	}

	void FixedUpdate(){
		if (grid.tension > 380.0f) {
			sucess = true;
		} else {
			sucess = false;
		}
		
	}

	public void FinalizeGoal(){
		sucess = true;
	}

	public string GetName(){
		return goalName;
	}

	public string GetDesc(){
		return goalDesc;
	}

	public string GetLongDesc(){
		return goalLongDesc;
	}

	public bool GetCompleted(){
		return sucess;
	}
}
