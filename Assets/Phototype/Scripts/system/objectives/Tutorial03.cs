﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tutorial03 : MonoBehaviour, IGoal {
	bool sucess = false;
	string goalName = "Limpeza";
	string goalDesc = "Limpe alguns painéis no quintal";
	string goalLongDesc = "Limpe pelo menos dois painéis sujos utilizando a sua arma";

	public SolarPanel p1, p2;

	private MessagesController messages;

	void Start(){

		messages = GameObject.FindGameObjectWithTag ("Canvas").GetComponent<MessagesController> ();

		List<string> dialog = new List<string> ();
		dialog.Add ("Usando o gatilho alternativo da arma é possível limpar os paineis sujos");
		dialog.Add ("Precisamos de ajuda pra limpar alguns painéis no nosso quintal");
		dialog.Add ("A limpeza é muito importante pra garantir o bom funcionamento do sistema fotovoltaico");
		dialog.Add ("E parece que nosso sistema de energia já anda bastante comprometido");
		messages.SendDialog (dialog);
	}

	public IEnumerator StartGoal(System.Action<bool> done){
		while (true) {
			if (sucess) {
				done (true);
				break;
			}
			//Debug.Log ("Verificando " + sucess);
			yield return new WaitForSeconds (1);
		}
	}

	void FixedUpdate(){

		if(p1.cleaning == 1 && p2.cleaning == 1){
			sucess = true;
		}
	}

	public void FinalizeGoal(){
		sucess = true;
	}

	public string GetName(){
		return goalName;
	}

	public string GetDesc(){
		return goalDesc;
	}

	public string GetLongDesc(){
		return goalLongDesc;
	}

	public bool GetCompleted(){
		return sucess;
	}
}
