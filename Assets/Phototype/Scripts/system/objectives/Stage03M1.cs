﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Stage03M1 : MonoBehaviour, IGoal {

	bool sucess = false;
	string goalName = "Para o céu";
	string goalDesc = "Acerte os painéis solares na melhor posição";
	string goalLongDesc = "Posicione corretamente os painéis solares através de seu menu para alcançar mais potência";

	public SolarGrid grid;

	void Start(){
		goalName = "Para o céu - " + grid.name;
	}

	public IEnumerator StartGoal(System.Action<bool> done){
		while (true) {
			if (sucess) {
				done (true);
				break;
			}
			//Debug.Log ("Verificando " + sucess);
			yield return new WaitForSeconds (1);
		}
	}

	void FixedUpdate(){
		if(grid.current > 100f){
			sucess = true;
		}

	}

	public void FinalizeGoal(){
		sucess = true;
	}

	public string GetName(){
		return goalName;
	}

	public string GetDesc(){
		return goalDesc;
	}

	public string GetLongDesc(){
		return goalLongDesc;
	}

	public bool GetCompleted(){
		return sucess;
	}
}
