﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TutorialEnd : MonoBehaviour, IGoal {
	
	bool sucess = false;
	string goalName = "Ponto de inserção";
	string goalDesc = "Utilize o teletransporte";
	string goalLongDesc = "Utilize o teletransporte e escolha uma localidade para iniciar a sua aventura";

	public TeleportController teleport;
	public DoorHandler door;

	private MessagesController messages;

	void Start(){

		messages = GameObject.FindGameObjectWithTag ("Canvas").GetComponent<MessagesController> ();

		List<string> dialog = new List<string> ();
		dialog.Add ("Muito bom! Vejo que você está preparado para defender nossos sistemas");
		dialog.Add ("Use o teletransporte, mas por favor, tenha cuidado!");
		dialog.Add ("Não sabemos o que te aguarda em nenhuma das plantas fotovoltaicas");
		messages.SendDialog (dialog);
	}

	public IEnumerator StartGoal(System.Action<bool> done){

		//Open the door
		door.active = true;
		door.OpenDoor();

		while (true) {
			if (sucess) {
				done (true);
				break;
			}
			//Debug.Log ("Verificando " + sucess);
			yield return new WaitForSeconds (1);
		}
	}

	void FixedUpdate(){
		if(teleport.isUsed){
			sucess = true;
		}
	}

	public void FinalizeGoal(){
		sucess = true;
	}

	public string GetName(){
		return goalName;
	}

	public string GetDesc(){
		return goalDesc;
	}

	public string GetLongDesc(){
		return goalLongDesc;
	}

	public bool GetCompleted(){
		return sucess;
	}

}
