﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tutorial01 : MonoBehaviour, IGoal {

	bool sucess = false;
	string goalName = "Mexendo os esqueletos";
	string goalDesc = "Movimente o personagem";
	string goalLongDesc = "Utilize as teclas W, A, S, D ou as teclas direcionais para movimentar o seu personagem";

	Vector3 lastPosition;
	float distance = 0;

	public DoorHandler door;
	public Transform player;

	private MessagesController messages;

	void Start(){

		lastPosition = player.position;

		messages = GameObject.FindGameObjectWithTag ("Canvas").GetComponent<MessagesController> ();

		List<string> dialog = new List<string> ();
		dialog.Add ("Olá! Que bom você aceitou nossa missão");
		dialog.Add ("Precisamos reestabelecer o funcionamento de 3 plantas fotovoltaicas");
		dialog.Add ("Mas antes será necessário alguns procedimentos");
		messages.SendDialog (dialog);
	}

	public IEnumerator StartGoal(System.Action<bool> done){
		door.active = false;
		door.BlockDoor ();

		while (true) {
			if (sucess) {
				done (true);
				break;
			}
			//Debug.Log ("Verificando " + sucess);
			yield return new WaitForSeconds (1);
		}
	}

	void FixedUpdate(){

		distance += Vector3.Distance(lastPosition, player.position);
		lastPosition = player.position;

		Debug.Log ("Distancia: " + distance);

		if(distance > 10f){
			sucess = true;
		}

	}

	public void FinalizeGoal(){
		sucess = true;
	}

	public string GetName(){
		return goalName;
	}

	public string GetDesc(){
		return goalDesc;
	}

	public string GetLongDesc(){
		return goalLongDesc;
	}

	public bool GetCompleted(){
		return sucess;
	}
		
}
