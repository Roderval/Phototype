﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tutorial02 : MonoBehaviour, IGoal {
	bool sucess = false;
	string goalName = "Gatilho";
	string goalDesc = "Encontre e colete a arma";
	string goalLongDesc = "Colete a arma que encontra-se próximo à mesa do cientista";

	public GameObject gun;
	private MessagesController messages;

	void Start(){

		messages = GameObject.FindGameObjectWithTag ("Canvas").GetComponent<MessagesController> ();

		List<string> dialog = new List<string> ();
		dialog.Add ("Agora você precisa encontrar a nossa nova mais nova invenção");
		dialog.Add ("É uma arma que vai te ajudar limpar os painéis e a espantar os aliens");
		dialog.Add ("Ela está em cima da mesa do laboratório");
		messages.SendDialog (dialog);
	}

	public IEnumerator StartGoal(System.Action<bool> done){
		while (true) {
			if (sucess) {
				done (true);
				break;
			}
			//Debug.Log ("Verificando " + sucess);
			yield return new WaitForSeconds (1);
		}
	}

	void FixedUpdate(){
		if(gun.gameObject.activeInHierarchy){
			sucess = true;
		}
	}

	public void FinalizeGoal(){
		sucess = true;
	}

	public string GetName(){
		return goalName;
	}

	public string GetDesc(){
		return goalDesc;
	}

	public string GetLongDesc(){
		return goalLongDesc;
	}

	public bool GetCompleted(){
		return sucess;
	}
}
