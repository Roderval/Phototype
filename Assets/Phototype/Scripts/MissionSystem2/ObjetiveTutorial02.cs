﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjetiveTutorial02 : Objective {

	public SolarPanel p1;
	public SolarPanel p2;

	public override IEnumerator ObjectiveTrigger(){
		Debug.Log ("Limpe paineis");
		ShowMessages ();

		while(!sucess){

			if (p1.cleaning == 1 && p2.cleaning == 1) {
				sucess = true;
				OnObjectiveSucess ();
				Debug.Log ("Finalizado");
				break;
			}

			yield return new WaitForSeconds (1.0f);
		}

		yield return null;
	}

	public void ShowMessages(){
		MessagesController messages = GameObject.FindGameObjectWithTag ("Canvas").GetComponent<MessagesController> ();

		List<string> dialog = new List<string> ();
		dialog.Add ("Usando o gatilho alternativo da arma é possível limpar os paineis sujos");
		dialog.Add ("Precisamos de ajuda pra limpar alguns painéis no nosso quintal");
		dialog.Add ("A limpeza é muito importante pra garantir o bom funcionamento do sistema fotovoltaico");
		dialog.Add ("E parece que nosso sistema de energia já anda bastante comprometido");
		messages.SendDialog (dialog);
	}
}
