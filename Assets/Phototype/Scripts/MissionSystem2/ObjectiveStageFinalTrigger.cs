﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectiveStageFinalTrigger : Objective {

	public Objective obj01;
	public Objective obj02;
	public Objective obj03;

	public override IEnumerator ObjectiveTrigger(){
		while(!sucess){

			if(obj01.sucess && obj02.sucess && obj03.sucess){
				sucess = true;
				Debug.Log ("Finalizado todos");
				OnObjectiveSucess ();
				break;
			}

			yield return new WaitForSeconds (1.0f);
		}

		yield return null;
	}

}
