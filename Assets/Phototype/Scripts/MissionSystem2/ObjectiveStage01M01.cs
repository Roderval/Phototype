﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectiveStage01M01 : Objective {

	public SolarGrid grid;

	public override IEnumerator ObjectiveTrigger(){
		while(!sucess){

			if ((grid.tension * grid.current) > 3000.0f) {
				sucess = true;
				OnObjectiveSucess ();
				Debug.Log ("Finalizado");
				break;
			}

			yield return new WaitForSeconds (1.0f);
		}

		yield return null;
	}
}
