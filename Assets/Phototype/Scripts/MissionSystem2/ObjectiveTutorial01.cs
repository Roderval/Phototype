﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectiveTutorial01 : Objective {

	public GameObject gun;

	public override IEnumerator ObjectiveTrigger(){
		Debug.Log ("Pickup the gun!");
		ShowMessages ();

		while(!sucess){

			if (gun.activeSelf) {
				sucess = true;
				OnObjectiveSucess ();
				Debug.Log ("Finalizado");
				break;
			}

			yield return new WaitForSeconds (1.0f);
		}

		yield return null;
	}

	public void ShowMessages(){
		MessagesController messages = GameObject.FindGameObjectWithTag ("Canvas").GetComponent<MessagesController> ();

		List<string> dialog = new List<string> ();
		dialog.Add ("Agora você precisa encontrar a nossa nova mais nova invenção");
		dialog.Add ("É uma arma que vai te ajudar limpar os painéis e a espantar os aliens");
		dialog.Add ("Ela está em cima da mesa do laboratório");
		messages.SendDialog (dialog);
	}

}
