﻿using System.Collections;
using UnityEngine;

public class MissionSystem2 : MonoBehaviour {

	public static bool stageClear = false;

	void Start(){

		StartCoroutine (StartMission());

	}

	private IEnumerator StartMission(){

		yield return new WaitForSeconds(2f);

		//Verifica se a missão já não foi completada
		GameSettings gs = GameSettings.instance;
		World world = GameObject.FindGameObjectWithTag ("World").GetComponent<World> ();
		bool missionCompleted = false;
		gs.missionCompletedList.TryGetValue (world.stageID, out missionCompleted);
		stageClear = missionCompleted;


		foreach(Transform t in gameObject.transform){
			Objective obj = t.GetComponent<Objective> ();

			if (!missionCompleted) {
				if(obj.active){
					StartCoroutine(obj.ObjectiveTrigger());
				}
			} else {
				if(obj.visible){
					obj.sucess = true;
					obj.active = true;
				}
			}
		}
	}

	void Update(){
		if(Input.GetKeyDown(KeyCode.F5)){
			foreach(Transform t in gameObject.transform){
				Objective obj = t.GetComponent<Objective> ();
				Debug.Log (obj.name + " foi " + obj.sucess);
			}
		}
	}

}
