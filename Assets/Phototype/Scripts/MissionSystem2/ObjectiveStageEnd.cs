﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectiveStageEnd : Objective {

	public TeleportController teleport;

	public override IEnumerator ObjectiveTrigger(){

		MissionSystem2.stageClear = true;
		Debug.Log ("Stage Clear");

		while(!sucess){

			if (teleport.isUsed) {
				sucess = true;
				OnObjectiveSucess ();
				SaveProgress ();

				Debug.Log ("Finalizado");
				break;
			}

			yield return new WaitForSeconds (1.0f);
		}

		yield return null;
	}

	private void SaveProgress(){
		SolarPlant plant = GameObject.FindGameObjectWithTag ("SolarPlant").GetComponent<SolarPlant> ();
		GameSettings gs = GameSettings.instance;
		World world = World.instance;
		gs.missionCompletedList.Add (world.stageID, true);
		float plantPower = (plant.current * plant.tension);
		gs.missionPower.Add(world.stageID, plantPower);
		float finalTime = Time.time - world.startTime;
		gs.missionTimes.Add (world.stageID, finalTime);
	}
}
