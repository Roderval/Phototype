﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectiveTutorial03 : Objective {

	public DoorHandler door;
	public TeleportController teleport;

	public override IEnumerator ObjectiveTrigger(){
		MissionSystem2.stageClear = true;

		Debug.Log ("Pegue o teleport");
		ShowMessages ();

		door.active = true;
		door.OpenDoor();

		while (true) {
			if(teleport.isUsed){
				sucess = true;
				OnObjectiveSucess ();
				SaveProgress ();

				break;
			}
			yield return new WaitForSeconds (1);
		}

		yield return null;
	}

	public void ShowMessages(){
		MessagesController messages = GameObject.FindGameObjectWithTag ("Canvas").GetComponent<MessagesController> ();

		List<string> dialog = new List<string> ();
		dialog.Add ("Muito bom! Vejo que você está preparado para defender nossos sistemas");
		dialog.Add ("Use o teletransporte, mas por favor, tenha cuidado!");
		dialog.Add ("Não sabemos o que te aguarda em nenhuma das plantas fotovoltaicas");
		messages.SendDialog (dialog);
	}

	private void SaveProgress(){
		GameSettings gs = GameSettings.instance;
		gs.missionCompletedList.Add (0, true);
	}
}
