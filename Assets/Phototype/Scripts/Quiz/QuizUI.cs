﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class QuizUI : MonoBehaviour {

	public Text title;
	public Transform optionsTransform;
	public Question question;
	public GameObject optionObject;

	public void StartQuiz(){

		title.text = question.questionName;

		//Initialize options
		foreach(Option op in question.options){
			GameObject go = Instantiate(optionObject, new Vector3(), new Quaternion(), optionsTransform);
			go.GetComponent<OptionUI>().optionText.text = op.text;
			go.GetComponent<OptionUI>().quizUI = this;
		}
	}

	public void CheckAnswer(string anwserText){
		bool success = false;
		foreach(Option op in question.options){
			if(op.isCorrect){
				if(anwserText == op.text){
					success = true;
					Quiz.instance.CorrectAnswer();
					Quit();
				}
			}
		}

		if(!success){
			Quiz.instance.FalseAnswer();
			Quit();
		}
	}

	public void Quit(){
		this.gameObject.SetActive(false);

		//Destroy all options
		foreach(Transform t in optionsTransform){
			Destroy(t.gameObject);
		}

		PauseMenu.instance.TransparentPause();
		
	}

}
