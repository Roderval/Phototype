﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

public class LocalizationManager : MonoBehaviour {

    public static LocalizationManager instance;
    private Dictionary<string, string> localizedText;
    private bool isReady = false;
    private string missingTextString = "Localized text not found";
    private QuizData quizData;

    void Awake()
    {
        if (instance == null)
        {
            instance = this;

            LoadLocalizedText("language_" + GetLanguage() + ".json");
            LoadQuizData("quiz_" + GetLanguage() + ".json");

            isReady = true;

        } else
        {
            Destroy(gameObject);
        }

        DontDestroyOnLoad(gameObject);
    }

    public string GetLanguage()
    {
        return PlayerPrefs.GetString("language", "default");
    }

    public void LoadQuizData(string fileName)
    {
        string filePath = Path.Combine(Application.streamingAssetsPath, fileName);

        if (File.Exists(filePath))
        {
            string dataAsJson = File.ReadAllText(filePath);
            quizData = JsonUtility.FromJson<QuizData>(dataAsJson);
            Debug.Log("Loaded quiz database (" + GetLanguage() + ") with " + quizData.questions.Length + " questions");
            Debug.Log(quizData.questions[0].questionName);
        } else
        {
            Debug.LogError("Cannot find quiz database (" + GetLanguage() + ") in path " + filePath);
        }
    }

    public void LoadLocalizedText(string fileName)
    {
        localizedText = new Dictionary<string, string>();
        string filePath = Path.Combine(Application.streamingAssetsPath, fileName);

        if (File.Exists(filePath))
        {
            string dataAsJson = File.ReadAllText(filePath);
            LocalizationData loadedData = JsonUtility.FromJson<LocalizationData>(dataAsJson);

            for (int i = 0; i < loadedData.items.Length; i++)
            {
                localizedText.Add(loadedData.items[i].key, loadedData.items[i].value);
            }

            Debug.Log("Loaded localization data (" + GetLanguage() + ") with " + localizedText.Count + " entries");
        } else
        {
            Debug.LogError("Cannot find localized text (" + GetLanguage() + ") in path " + filePath);
        }
    }

    public string GetLocalizedValue(string key)
    {
        if(localizedText.ContainsKey(key)){
            string result = missingTextString;
            localizedText.TryGetValue(key, out result);
            return result;
        }

        Debug.LogError("String key " + key + " not found!");
        return null;
        
    }

    public Question[] GetQuestions()
    {
        return quizData.questions;
    }

    public bool GetIsReady()
    {
        return isReady;
    }

}
