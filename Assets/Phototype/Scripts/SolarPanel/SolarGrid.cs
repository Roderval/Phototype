﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SolarGrid : MonoBehaviour {

	const float ELETRIC_UPDATE = 0.5f;

	public Transform solarList;

	public float tension = 0;
	public float current = 0;

	public bool isOn = true;

	void Start(){
		StartCoroutine(CalculatePower ());
	}

	private IEnumerator CalculatePower(){
		while (true) {
			current = int.MaxValue;
			tension = 0;

			if (isOn) {
				foreach (Transform panel in solarList.transform) {
                    //current += panel.GetComponent<SolarPanel> ().current;
                    current = Mathf.Min(panel.GetComponent<SolarPanel>().current, current);
                    tension += panel.GetComponent<SolarPanel> ().tension;
				}
			} else
            {
                current = 0;
                tension = 0;
            }
		
			yield return new WaitForSeconds (ELETRIC_UPDATE);
		}
	}

    public float GetPower()
    {
        return current * tension;
    }

}
