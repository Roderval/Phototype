﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PanelInteractable : InteractableObject {

	private CanvasController canvas;
	private SolarPanel panel;

	private bool isActive;

	void Start(){
		if(GameObject.FindGameObjectWithTag("Canvas") != null){
			canvas = GameObject.FindGameObjectWithTag ("Canvas").GetComponent<CanvasController> ();
		}
	}

	public override void triggerAction ()
	{
		canvas.OpenMainPanelMenu(gameObject.GetComponentInParent<SolarPanel> (), true);
	}
}
