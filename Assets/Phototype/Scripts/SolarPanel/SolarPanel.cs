﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SolarPanel : MonoBehaviour {

	//Constantes
	const float ELETRIC_UPDATE = 0.5f;

	//Variaveis diretas
	public int id = 0;
	public float tension = 0;
	public float current = 0;
	public float maxTension = 36.8f;
	public float maxCurrent = 8.69f;
	public float projectedIncidence = 0;
	public float cleaning = 0;
	public float inclination = 0;
	public float orientation = 0;
	public bool isOn = true;

	//Interacao no awake
	public bool makeDirty = false;
	public bool randomPosition = false;
	public bool startOff = false;

	//Utils
	private GameObject[] dirtCells = new GameObject[4];
	public AudioSource audioSource;
	public AudioClip audioLerp;
	public AudioClip audioLock;

	//GameObjects
	public Transform roll;
	public GameObject dirtyObject;
	public Transform dirty;

	//Variaveis Globais
	private World world;

	//LERPINGMOTOR
	private bool isLerping = false;
	private float startTime;
	private Quaternion startPosition;
	private Quaternion endPosition;
	private float timeTakenDuringLerp;

	void Awake(){
		world = GameObject.FindGameObjectWithTag ("World").GetComponent<World> ();

		if (makeDirty) {
			DirtyPanels ();
		}
		if (randomPosition) {
			//setFixedPosition (25, 40);
			SetWorstAngle ();
		} else {
			SetBestAngle ();
		}
		if (startOff) {
			isOn = false;
		}
	}

	void Start(){
		startTime = Time.time;
		StartCoroutine(CalculatePower ());
		AudioManager.instance.RegisterSpatialSource(audioSource);
	}

	void FixedUpdate(){
		RotorLerping ();
		//setBestAngle ();
	}

	public void setFixedPosition(float inclination, float orientation){
		this.inclination = inclination;
		this.orientation = orientation;
		roll.transform.rotation = Quaternion.Euler (new Vector3(0, orientation, inclination));
	}

	public void setPosition(float inclination, float orientation){
		this.inclination = inclination;
		this.orientation = orientation;
		StartRotor();
	}

	public void SetBestAngle(){
		roll.eulerAngles = CalculateBestAngle ();
		//roll.eulerAngles = CalculateRandomBestAngle ();
	}

	public void SetWorstAngle(){
		//roll.eulerAngles = CalculateWorstAngle ();
		roll.eulerAngles = CalculateRandomWorstAngle ();
	}

    //Calcula os parametros elétricos
	private IEnumerator CalculatePower(){
		while (true) {
			current = 0;
			tension = 0;

			if (isOn) {
				CalculateIncidence ();
				CalculateClean ();

				current = maxCurrent * projectedIncidence * cleaning * 1;
                //tension = maxTension * projectedIncidence * cleaning * 1;
                tension = maxTension;
			}

			yield return new WaitForSeconds (ELETRIC_UPDATE);
		}
	}

	//Calcula os parametros elétricos
	private void CalculateIncidence(){
		Transform sun = world.eulerSun;
		Transform obj = roll;
		//projectedIncidence = Vector3.Angle (sun.forward, obj.up) / 180;

		projectedIncidence = Vector3.Dot (obj.up, sun.forward) * -1;
		if(projectedIncidence < 0){
			projectedIncidence = 0;
		}

		//Debug.Log ("dot : " + projectedIncidence);
	}

	private Vector3 CalculateBestAngle(){

		//ORIENTACAO
		float bestOrientation = 0;
		if(Mathf.Sign(world.eulerSun.rotation.y) == 1){
			bestOrientation = 0;
		} else {
			bestOrientation = 180;
		}

		//INCLINACAO
		float bestInclination = 90 - world.eulerSun.eulerAngles.x;

		Vector3 bestAngle = new Vector3 (0, bestOrientation, Mathf.Clamp(bestInclination, 0, 50));
		this.inclination = (int) bestInclination;
		this.orientation = (int) bestOrientation;

		return bestAngle;
	}

	private Vector3 CalculateWorstAngle(){
		//ORIENTACAO
		float bestOrientation = 0;

		if(world.eulerSun.eulerAngles.y > 270){
			bestOrientation = world.eulerSun.eulerAngles.y - 270;
		} else {
			bestOrientation = world.eulerSun.eulerAngles.y + 90;
		}

		//INCLINACAO
		//float bestInclination = world.eulerSun.eulerAngles.x - 40;
		//O CALCULO ACIMA ESYA CORRETO MAS N EH USUAL
		float bestInclination = 50;

		Vector3 bestAngle = new Vector3 (0, bestOrientation, bestInclination);
		this.inclination = (int) bestInclination;
		this.orientation = (int) bestOrientation;

		return bestAngle;
	}

	private Vector3 CalculateRandomBestAngle(){

		//ORIENTACAO
		float bestOrientation = 0;
		if(world.eulerSun.eulerAngles.y < 90){
			bestOrientation = Mathf.Clamp(270 - world.eulerSun.eulerAngles.y + Random.Range(-5, 5), 0, 359);
		} else {
			bestOrientation = Mathf.Clamp(world.eulerSun.eulerAngles.y - 90 + Random.Range(-5, 5), 0, 359);;
		}

		//INCLINACAO
		float bestInclination = Mathf.Clamp(90 - world.eulerSun.eulerAngles.x + Random.Range(-5, 5), 0, 359);

		Vector3 bestAngle = new Vector3 (0, bestOrientation, bestInclination);
		this.inclination = (int) bestInclination;
		this.orientation = (int) bestOrientation;

		return bestAngle;
	}

	private Vector3 CalculateRandomWorstAngle(){
		//ORIENTACAO
		float bestOrientation = 0;
		if(Mathf.Sign(world.eulerSun.rotation.y) == 1){
			bestOrientation = 180;
		} else {
			bestOrientation = 0;
		}

		//INCLINACAO
		float bestInclination = Random.Range(38, 50);

		Vector3 bestAngle = new Vector3 (0, bestOrientation, bestInclination);
		this.inclination = (int) bestInclination;
		this.orientation = (int) bestOrientation;

		return bestAngle;
	}

	private void CalculateClean(){
		cleaning = 0;

		foreach(GameObject cell in dirtCells){
			if(cell == null){
				cleaning += 0.25f;
			}
		}
	}

	//Metodos para interação do painel
	public void DirtyPanels(){
		if(dirty.childCount == 0){
			Vector3[] pos = new Vector3[4];
			pos [0] = new Vector3 (0.25f, 0, 0.25f);
			pos [1] = new Vector3 (0.25f, 0, -0.25f);
			pos [2] = new Vector3 (-0.25f, 0, 0.25f);
			pos [3] = new Vector3 (-0.25f, 0, -0.25f);

			int i = 0;
			foreach(Vector3 p in pos){
				dirtCells[i] = Instantiate(dirtyObject, dirty, false);
				dirtCells[i].transform.localPosition = p;
				i++;
			}
		}
	}

	//LERPING MOTOR
	public void StartRotor(){
		isLerping = true;
		startTime = Time.time;
		startPosition = roll.rotation;
		endPosition = Quaternion.Euler(new Vector3(0, orientation, inclination));

		Debug.Log (endPosition);

		//timeTakenDuringLerp = (endPosition.eulerAngles - startPosition.eulerAngles).magnitude * 0.1f;
		timeTakenDuringLerp = Quaternion.Angle(startPosition, endPosition) * 0.1f;

		Debug.Log (timeTakenDuringLerp);

		audioSource.clip = audioLerp;
		audioSource.Play();
	}

	private void RotorLerping(){
		if(isLerping){
			float timeSinceStarted = Time.time - startTime;
			float percentageComplete = timeSinceStarted / timeTakenDuringLerp;
			roll.rotation = Quaternion.Lerp(startPosition, endPosition, percentageComplete);
			//roll.eulerAngles = Vector3.Lerp (startPosition, endPosition, percentageComplete);

			if(percentageComplete >= 1f){
				isLerping = false;
				audioSource.Stop ();
				audioSource.clip = audioLock;
				audioSource.Play ();
			}
		}
	}

}
