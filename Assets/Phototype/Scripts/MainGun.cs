﻿using UnityEngine;
using System.Collections;

public class MainGun : MonoBehaviour
{
	//Laser Gun Variables
	public int laserDamage = 10;
	public float laserFireRate = .25f;
	public float laserRange = 50;
	public float laserForce = 100f;
	public Transform laserNozzle;
	public AudioSource laserAudio;
	public LineRenderer laserLine;
	public GameObject shotEffect;

	private WaitForSeconds laserDuration = new WaitForSeconds(.05f);

	//Cleaner Gun Variables
	public int cleanerDamage = 0;
	public int cleanerRange = 3;
	public Transform cleanerNozzle;
	public AudioSource cleanerAudio;
	public ParticleSystem cleanerParticles;

	public Camera fpsCam;
	private float nextFire;

	private Animator anim;

	void Start(){
		anim = gameObject.GetComponent<Animator> ();

		AudioManager.instance.RegisterSpatialSource(cleanerAudio);
		AudioManager.instance.RegisterSpatialSource(laserAudio);
	}

	void Update(){

		Vector3 rayOrigin = fpsCam.ViewportToWorldPoint (new Vector3 (0.5f, 0.5f, 0));

		if(Time.timeScale == 1){
			LaserGun (rayOrigin);
			CleanerGun (rayOrigin);
		}

		//update linerenderer (bug?)
		laserLine.SetPosition (0, laserNozzle.position);
		laserLine.SetPosition(1, rayOrigin + (fpsCam.transform.forward * laserRange));
}

	private IEnumerator LaserEffect(){
		laserAudio.Play ();
		laserLine.enabled = true;
		yield return laserDuration;
		laserLine.enabled = false;
	}

	private void LaserGun(Vector3 rayOrigin){
		//lasergun
		if ((Input.GetButtonDown ("Fire1") || Input.GetAxis("Fire1") > 0.8f) && Time.time > nextFire) {
			//Debug.Log ("Laser has been fired");
			nextFire = Time.time + laserFireRate;
			StartCoroutine (LaserEffect());
			RaycastHit hit;

			//laser fire
			laserLine.SetPosition (0, laserNozzle.position);
			//TODO: BUG - o raycast está parando em colliders que são apenas triggers
			if(Physics.Raycast(rayOrigin, fpsCam.transform.forward, out hit, laserRange)){
				laserLine.SetPosition(1, hit.point);

				//spark if not enemy
				if(!hit.collider.CompareTag("Respawn")){
					GameObject spark = Instantiate (shotEffect, hit.point, new Quaternion());
					Destroy (spark, 1f);
				}

				Destroyable health = hit.collider.GetComponent<Destroyable> ();
				//damage
				if(health != null){
					health.Damage (laserDamage);
				}
				//recoil
				if(hit.rigidbody != null){
					hit.rigidbody.AddForce (-hit.normal * laserForce);
				}
			} else {
				laserLine.SetPosition(1, rayOrigin + (fpsCam.transform.forward * laserRange));
			}

			anim.SetTrigger ("Shoot");
		}
	}

	private void CleanerGun(Vector3 rayOrigin){
		//Cleaner
		//cleanergun
		if(Input.GetButtonDown("Fire2")){
			cleanerAudio.loop = true;
			cleanerAudio.Play ();
		}

		if ((Input.GetButtonDown ("Fire2") || Input.GetAxis("Fire2") > 0.8f) && Time.time > nextFire) {
			RaycastHit hit;
			//StartCoroutine (CleanerEffect ());
			cleanerParticles.Play ();

			//cleaner fire
			if (Physics.Raycast (rayOrigin, fpsCam.transform.forward, out hit, cleanerRange)) {
				//Debug.Log ("Atingiu " + hit.transform.name);
				//recoil
				if (hit.rigidbody != null) {
					hit.rigidbody.AddForce (-hit.normal * laserForce);
				}

				//Se atingiu um painel fotovoltaico
				if(hit.transform.GetComponentInParent<SolarPanel>() != null){

					//Mostra a barra de limpeza
					//canvas.ShowCleaningBar (hit.transform.GetComponentInParent<SolarPanel>());
				}

				//cleaning
				if (hit.transform.CompareTag ("DirtCell")) {

					Renderer rend = hit.transform.GetComponent<Renderer> ();
					//Debug.Log (Time.deltaTime);
					rend.material.color -= new Color (0, 0, 0, Time.deltaTime);
					if (rend.material.color.a <= 0.2f) {
						Destroy (hit.transform.gameObject);
					}
				}

				//Fire
				if (hit.transform.CompareTag ("Fire")) {
					FireController fire = hit.transform.GetComponent<FireController> ();
					fire.Clean (0.1f);
				}

			}
		} else {
			cleanerParticles.Stop ();
			cleanerAudio.Stop ();
		}
	}

}

