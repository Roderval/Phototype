﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TextVoltage : MonoBehaviour {

	private TextMesh text;
	public GridController grid;

	void Start(){
		text = gameObject.GetComponent<TextMesh> ();
	}

	void FixedUpdate () {
		float power = grid.finalCurrent * grid.finalTension;
		text.text = grid.finalTension + "v\n" + grid.finalCurrent + "a\n" + power/1000 + "kW";
	}
}
