﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectiveTrigger : MonoBehaviour {

	public GameObject selectedCluster;

	void OnTriggerEnter(Collider other){
		if(other.CompareTag("Player")){
			selectedCluster.SetActive (true);
		}
	}

	void OnTriggerExit(Collider other){
		if(other.CompareTag("Player")){
			selectedCluster.SetActive (false);
		}
	}
}
