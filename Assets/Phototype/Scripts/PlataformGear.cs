﻿using System.Collections;
using UnityEngine;

public class PlataformGear : MonoBehaviour {
	
	public Animator anim;


	void Start(){
		StartCoroutine (StartPlataformAnimation());
	}

	private IEnumerator StartPlataformAnimation(){
		yield return new WaitForSeconds(5);

		anim.enabled = true;

		yield return new WaitForSeconds (10);

		anim.enabled = false;
	}
}
