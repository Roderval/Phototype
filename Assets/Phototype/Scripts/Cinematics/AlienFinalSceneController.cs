﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.SceneManagement;

public class AlienFinalSceneController : MonoBehaviour {

	public NavMeshAgent nav;
	public Animator anim;
	public Transform mainCamera;
	public Renderer rend;
	public GameObject teleportSmoke;

	public GameObject stage01;
	public GameObject stage02;
	public GameObject stage03;
	public GameObject stage04;
	public GameObject stage05;
	public GameObject stage06;


	public GameObject background;
	public GameObject finalBackground;
	public GameObject creditsSequence;
	public Material skybox;

	public Transform[] dest;

	private bool stage01Completed = false;
	private bool stage02Completed = false;
	private bool stage03Completed = false;
	private bool stage04Completed = false;
	private bool stage05Completed = false;
	private bool stage06Completed = false;

	private AudioSource music;

	void Start(){
        AudioManager.instance.StopEffect("efx_typing");
		StartCoroutine (PlayMusic());
		StartCoroutine(Spawn ());
		nav.SetDestination (dest[0].position);
		creditsSequence.SetActive (true);
	}

	void Update(){
		if (!stage06Completed) {
			if (!stage05Completed) {
				mainCamera.LookAt (gameObject.transform.position + new Vector3 (0, 1, 0));
				mainCamera.Translate (Vector3.right * Time.deltaTime * 0.2f);
			} else {
				mainCamera.LookAt (gameObject.transform.position + new Vector3 (0, 1, 0));
				mainCamera.Translate (Vector3.up * Time.deltaTime * 0.2f);
			}
		} else {
			mainCamera.LookAt (gameObject.transform.position + new Vector3 (0, 1, 0));
			mainCamera.Translate (Vector3.right * Time.deltaTime * 0.2f);
			mainCamera.GetComponent<Camera> ().fieldOfView += Time.deltaTime * 2f;
		}
	}

	void FixedUpdate(){
		if (nav.velocity.magnitude > 0) {
			anim.SetBool ("moving", true);
		} else {
			anim.SetBool ("moving", false);
		}

		if(!stage01Completed && Vector3.Distance(transform.position, dest[0].position) < 1f){
			stage01Completed = true;
			nav.enabled = false;
			StartCoroutine (MoveToStage02());
		}

		if(!stage02Completed && Vector3.Distance(transform.position, dest[2].position) < 1f){
			stage02Completed = true;
			nav.enabled = false;
			StartCoroutine (MoveToStage03());
		}

		if(!stage03Completed && Vector3.Distance(transform.position, dest[4].position) < 1f){
			stage03Completed = true;
			nav.enabled = false;
			StartCoroutine (MoveToStage04());
		}

		if(!stage04Completed && Vector3.Distance(transform.position, dest[6].position) < 1f){
			stage04Completed = true;
			nav.enabled = false;
			StartCoroutine (MoveToStage05());
		}

		if(!stage05Completed && Vector3.Distance(transform.position, dest[8].position) < 1f){
			stage05Completed = true;
			nav.enabled = false;
			StartCoroutine (MoveToStage06());
		}
	}

	private IEnumerator PlayMusic(){
		yield return new WaitForSeconds(2f);
		music = AudioManager.instance.PlayMusic ("music_end");
	}

	private IEnumerator MoveToStage02(){
		Debug.Log ("Indo para estagio 2");
		background.SetActive (false);
		background.SetActive (true);
		yield return new WaitForSeconds (3f);
		stage01.SetActive (false);
		stage02.SetActive (true);
		StartCoroutine(Spawn ());
		transform.position = dest [1].position;
		nav.enabled = true;
		nav.SetDestination(dest[2].position);
	}

	private IEnumerator MoveToStage03(){
		Debug.Log ("Indo para estagio 2");
		background.SetActive (false);
		background.SetActive (true);
		yield return new WaitForSeconds (3f);
		stage02.SetActive (false);
		stage03.SetActive (true);
		StartCoroutine(Spawn ());
		transform.position = dest [3].position;
		nav.enabled = true;
		nav.SetDestination(dest[4].position);
	}

	private IEnumerator MoveToStage04(){
		background.SetActive (false);
		background.SetActive (true);
		Debug.Log ("Indo para estagio 2");
		yield return new WaitForSeconds (3f);
		stage03.SetActive (false);
		stage04.SetActive (true);
		StartCoroutine(Spawn ());
		transform.position = dest [5].position;
		nav.enabled = true;
		nav.SetDestination(dest[6].position);
	}

	private IEnumerator MoveToStage05(){
		Debug.Log ("Indo para estagio 2");
		background.SetActive (false);
		background.SetActive (true);
		yield return new WaitForSeconds (3f);
		stage04.SetActive (false);
		stage05.SetActive (true);
		StartCoroutine(Spawn ());
		transform.position = dest [7].position;
		nav.enabled = true;
		nav.SetDestination(dest[8].position);
	}

	private IEnumerator MoveToStage06(){
		background.SetActive (false);
		background.SetActive (true);
		yield return new WaitForSeconds (3f);
		StartCoroutine(Teleport ());
	}

	private IEnumerator Spawn(){
		Shader originalShader = rend.material.shader;
		rend.material.shader = Shader.Find ("Particles/Additive");
		yield return new WaitForSeconds (0.5f);
		rend.material.shader = originalShader;
	}

	public IEnumerator Teleport(){
		transform.position = dest [9].position;
		RenderSettings.skybox = skybox;
		DynamicGI.UpdateEnvironment ();
		stage06Completed = true;
		yield return new WaitForSeconds (music.clip.length - music.time);
		finalBackground.SetActive (true);
		yield return new WaitForSeconds (5f);
		SceneManager.LoadScene ("MainMenu");
	}
}
