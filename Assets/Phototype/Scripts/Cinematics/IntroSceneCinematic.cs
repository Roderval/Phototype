﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class IntroSceneCinematic : MonoBehaviour {

	public MessageUI mainText;
	public GameObject continueText;
	public GameObject loadingScreen;

	private bool continuePressed = false;
	private bool continueEnabled = false;

	void Start() {

        Time.timeScale = 1;

        AudioManager.instance.PlayMusic(null);

		StartCoroutine (ContinueTextToggle());

        List<string> msg = new List<string> ();
		msg.Add (LocalizationManager.instance.GetLocalizedValue("mission.intro.dialog01"));
		msg.Add (LocalizationManager.instance.GetLocalizedValue("mission.intro.dialog02"));
		msg.Add (LocalizationManager.instance.GetLocalizedValue("mission.intro.dialog03"));
		msg.Add (LocalizationManager.instance.GetLocalizedValue("mission.intro.dialog04"));
		msg.Add (LocalizationManager.instance.GetLocalizedValue("mission.intro.dialog05"));

        mainText.setPool (msg);
		mainText.gameObject.SetActive (true);

    }

	void Update(){
		if(continueEnabled){
			if(Input.GetButtonDown("Jump")){
				continuePressed = true;
			}
		}

		if(continuePressed){
			LoadingScreen ();
			AudioManager.instance.StopEffect ("efx_typing");
            StopAllCoroutines();
			SceneManager.LoadScene ("ControlRoom");
		}
    }

	private IEnumerator ContinueTextToggle(){
        yield return new WaitForSeconds (2f);
        continueText.SetActive (true);
        continueEnabled = true;
    }

	private void LoadingScreen(){
		loadingScreen.SetActive (true);
	}

}
