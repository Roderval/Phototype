﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickUpGun : InteractableObject {

	public GameObject gun;

	public override void triggerAction ()
	{
		gun.SetActive (true);
		AudioManager.instance.PlayEffect ("efx_pickupgun");
		gameObject.transform.GetChild(0).gameObject.SetActive(false);
		isActivated = false;
		Destroy (gameObject, 5);
	}

}
