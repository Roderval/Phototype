﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InteractableNPC : InteractableObject {

	private AudioSource voice;
	private NPCController npcController;
	private bool range;

	public AudioClip helloSound;

	void Start(){
		voice = gameObject.GetComponentInChildren<AudioSource>();
		npcController = gameObject.GetComponent<NPCController>();

		AudioManager.instance.RegisterSpatialSource(voice);
	}

	public override void triggerAction(){
		Debug.Log ("NPC interaction");
		Say(helloSound);
		npcController.Wave ();
	}

	private void Say(AudioClip sound){
		Debug.Log ("voice: " + voice + " / sound: " + sound + " / play: " + voice.isPlaying);
		if(voice != null && sound != null && !voice.isPlaying){
			Debug.Log ("Playing voice");
			voice.clip = sound;
			voice.Play ();
		}
	}

}
